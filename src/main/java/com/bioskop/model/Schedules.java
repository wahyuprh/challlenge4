package com.bioskop.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Setter
@Getter
@Entity(name = "schedules")
public class Schedules {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "schedules_id")
    private Integer schedulesId;

    @ManyToOne(targetEntity = Films.class)
    @JoinColumn(name = "film_id", nullable = false)
    private Films filmId;

    @Column(name = "date_show")
    private LocalDate dateShow;

    @Column(name = "start_time")
    private LocalTime startTime;

    @Column(name = "end_time")
    private LocalTime endTime;

    @Column(name = "price")
    private Long price;
}