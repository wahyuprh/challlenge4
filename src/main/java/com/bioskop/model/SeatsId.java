package com.bioskop.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@Embeddable
public class SeatsId implements Serializable {
    private Integer studioName;
    private String seatsCode;
}
