package com.bioskop.repository;

import com.bioskop.model.Films;
import com.bioskop.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface FilmsRepository extends JpaRepository<Films, Integer> {

    public Films findByFilmCode(String filmCode);

    public Films findByFilmId(Integer filmId);

    @Query(value = "select * from films f where f.is_show = true", nativeQuery = true)
    List<Films> findAllByIsShowTrue();
}
