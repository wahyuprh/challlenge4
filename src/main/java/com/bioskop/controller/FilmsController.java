package com.bioskop.controller;

import com.bioskop.service.FilmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class FilmsController {

    @Autowired
    private FilmsService filmsService;

    // Add Film
    public String addFilm(String filmCode, String filmName, Boolean isShow) {
        filmsService.addFilm(filmCode, filmName, isShow);
        return "Add Film Success!";
    }

    // Update Film by filmCode
    public String updateFilmName(String filmCode, String newFilmName, Boolean newIsShow) {
        filmsService.updateFilmNameByFilmCode(filmCode, newFilmName, newIsShow);
        return "Update Film Success!";
    }

    //Delete Film
    public String deleteFilm(Integer filmId){
        filmsService.deleteFilmById(filmId);
        return "Delete Film Succes!";
    }

    public void findAllByIsShowTrue(){
        filmsService.getFilmIsShowtrue();
    }

    public String addSchedules(Integer filmId, String dateShow, String  startTime, String  endTime, String price){
        filmsService.addSchedule(filmId,dateShow,startTime,endTime,price);
        return "Save Schedule Success!";
    }

    public void findSchedulesByFilmId(Integer filmId){
        filmsService.getSchedulesFilms(filmId);
    }

}
