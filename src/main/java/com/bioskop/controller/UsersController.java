package com.bioskop.controller;

import com.bioskop.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

//Jembatan dari service ke unit test
@Controller
public class UsersController {

    @Autowired
    private UsersService usersService;

    //Add User
    public String addUser(String username, String email, String password) {
        usersService.addUser(username, email, password);
        return "Add User Success!";
    }

    //Update User
    public String updateUser(String username, String newUsername, String newEmail, String newPassword) {
        usersService.updateUser(username, newUsername, newEmail, newPassword);
        return "Update User Success!";
    }

    //Delete User
    public void deleteUser(Integer userId) {
        usersService.deleteUser(userId);
    }
}
