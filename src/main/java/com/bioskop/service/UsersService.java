package com.bioskop.service;

import com.bioskop.model.Users;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface UsersService {

    void addUser(String username, String email, String password);

    void updateUser(String username, String newUsername, String newEmail, String newPassword);

    void deleteUser(Integer userId);
}