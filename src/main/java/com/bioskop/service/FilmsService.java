package com.bioskop.service;

import org.springframework.stereotype.Service;

@Service
public interface FilmsService {
    void addFilm(String filmCode, String filmName, Boolean isShow);

    void updateFilmNameByFilmCode(String filmCode, String newFilmName, Boolean newIsShow);

    void deleteFilmById(Integer filmId);

    void getFilmIsShowtrue();

    void addSchedule(Integer filmId, String dateShow, String startTime, String endTime, String price);

    void getSchedulesFilms(Integer filmId);
}