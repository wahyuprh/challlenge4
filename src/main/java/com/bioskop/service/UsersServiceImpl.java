package com.bioskop.service;

import com.bioskop.model.Users;
import com.bioskop.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public void addUser(String username, String email, String password) {
        Users users = new Users();
        // Insert Data
        users.setUsername(username);
        users.setEmail(email);
        users.setPassword(password);
        usersRepository.save(users);
    }

    @Override
    public void updateUser(String username, String newUsername, String newEmail, String newPassword) {
        Users user = usersRepository.findByUsername(username);
        user.setUsername(newUsername);
        user.setEmail(newEmail);
        user.setPassword(newPassword);
        usersRepository.save(user);
    }

    @Override
    public void deleteUser(Integer userId) {
        usersRepository.deleteById(userId);
    }
}