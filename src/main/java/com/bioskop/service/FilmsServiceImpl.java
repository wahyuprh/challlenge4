package com.bioskop.service;

import com.bioskop.model.Films;
import com.bioskop.model.Schedules;
import com.bioskop.repository.FilmsRepository;
import com.bioskop.repository.SchedulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Service
public class FilmsServiceImpl implements FilmsService {

    @Autowired
    private FilmsRepository filmsRepository;

    @Autowired
    private SchedulesRepository schedulesRepository;

    @Override
    public void addFilm(String filmCode, String filmName, Boolean isShow) {
        Films film = new Films();
        film.setFilmCode(filmCode);
        film.setFilmName(filmName);
        film.setIsShow(isShow);
        filmsRepository.save(film);
    }

    @Override
    public void updateFilmNameByFilmCode(String filmCode, String newFilmName, Boolean newIsShow) {
        Films film = filmsRepository.findByFilmCode(filmCode);
        film.setFilmName(newFilmName);
        film.setIsShow(newIsShow);
        filmsRepository.save(film);
    }

    @Override
    public void deleteFilmById(Integer filmId) {
        filmsRepository.deleteById(filmId);
    }

    @Override
    public void getFilmIsShowtrue() {
        List<Films> filmsList = filmsRepository.findAllByIsShowTrue();
        filmsList.forEach(films -> System.out.println(filmsList.toString()));
    }

    @Override
    public void addSchedule(Integer filmId, String dateShow, String startTime, String endTime, String price) {
        Schedules schedules = new Schedules();
        schedules.setDateShow(LocalDate.parse(dateShow));
        schedules.setStartTime(LocalTime.parse(startTime));
        schedules.setEndTime(LocalTime.parse(endTime));
        Films films = filmsRepository.findByFilmId(filmId);
        schedules.setFilmId(films);
        schedulesRepository.save(schedules);

    }

    @Override
    public void getSchedulesFilms(Integer filmId) {
        List<Schedules> schedulesList = schedulesRepository.findSchedulesByFilmId(filmId);
        schedulesList.forEach(schedules -> System.out.println(schedulesList.toString()));
    }
}