package com.bioskop;

import com.bioskop.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BioskopApplication {

    public static void main(String[] args) {
        SpringApplication.run(BioskopApplication.class, args);
    }
}
