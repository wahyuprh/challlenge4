package com.bioskop.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FilmsControllerTest {

    @Autowired
    private FilmsController filmsController;

    @Test
    @DisplayName("Add Film")
    public void addFilm() {
        String response = filmsController.addFilm("AVGULKK67", "BAJAJ BAJURI", false);
        Assertions.assertEquals("Add Film Success!", response);
    }

    @Test
    @DisplayName("Update Film")
    void updateFilmName() {
        String response = filmsController.updateFilmName("AVGULKK67", "Dear Nathan", false);
        Assertions.assertEquals("Update Film Success!", response);
    }

    @Test
    @DisplayName("Delete Film")
    void deleteFilm() {
        String response = filmsController.deleteFilm(28);
        Assertions.assertEquals("Delete Film Succes!", response);
    }

    @Test
    @DisplayName("Film is Show")
    void findAllFilmIsShowTest() {
        filmsController.findAllByIsShowTrue();
    }
    @Test
    @DisplayName("Save Schedules")
    void saveSchedulesTest(){
        String response = filmsController.addSchedules(25, "2022-10-20", "09:00:00", "10:25:00","35000");
        Assertions.assertEquals("Save Schedule Success!", response);
    }
    @Test
    @DisplayName("Positive Test Show Schedules")
    void findSchedulesByFilmIdTest(){
        filmsController.findSchedulesByFilmId(25);

    }
}