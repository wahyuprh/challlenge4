package com.bioskop.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UsersControllerTest {

    @Autowired
    private UsersController usersController;

    @Test
    @DisplayName("Add User")
    public void addUser() {
        String response = usersController.addUser("Bili", "Bili@email.com", "Bilipass");
        Assertions.assertEquals("Add User Success!", response);
    }

    @Test
    @DisplayName("Update User")
    public void updateUser() {
        usersController.updateUser("Bili", "wahyu", "wahyu@email.com", "wahyuu");
    }

    @Test
    @DisplayName("Delete User")
    void deleteUser() {
        usersController.deleteUser(24);
    }
}
